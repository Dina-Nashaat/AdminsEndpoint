# README

Available Endpoints for Uber Driver Service:

Make sure to setup [Driver Endpoint](https://gitlab.com/Dina-Nashaat/DriversEndpoint) because it contains migration files

# How to run on local machine: 
- Make sure rails is setup on your machine
[on Windows](https://gorails.com/setup/windows/10)
[on Ubuntu](https://gorails.com/setup/ubuntu/16.04)
- clone the repo
- if you're using rvm, make sure your ruby version is set to 2.5.1
- Run bundle install
- Run rails s -p 5000 to run it on localhost:5000

## Sign up a new user
### GET /trips
It can receive no or 1 param with the following key,value pair ``` duration : "month/year/all"```


[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/96bddbc1ce03f02fc5fa)
You could open it using postman app, or open the web view, download the json file, and import from postman on desktop.