class TripsController < ApplicationController
  def index
    if params[:duration]
      @trips = trips_per(params[:duration].to_sym)
    else
      @trips = all_trips
    end

    render json: @trips.as_json(:except => :id), status: :ok
  end

  private

  def where_clause
    {
      month: {'trips.created_at' => Time.zone.now.beginning_of_month..Time.zone.now.end_of_month},
      year: {'trips.created_at' => Time.zone.now.beginning_of_year..Time.zone.now.end_of_year},
      all: nil
    }
  end

  def trips_per(duration)
    query(where_clause[duration])
  end

  def all_trips
    trips = {}
    where_clause.each do |key, clause|
      trips[key] = query(clause)
    end
    trips
  end

  def query(clause)
    all_trips = Trip.all.count
    percentage = "count(trips.id)::decimal/#{all_trips}::decimal"
    return Trip.select("user_id, users.email, #{percentage} as percentage, trips.created_at")
      .joins(:user)
      .group("trips.user_id, trips.created_at, users.email")
      .order("trips.user_id")
      .having("#{percentage} >= 0.1")
      .where(clause)
  end

end